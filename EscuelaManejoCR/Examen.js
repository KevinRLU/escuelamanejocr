/* Global variables */

let SCORE;
let RAND_QUES;
let NUMS;

/*************************** */

const scoreBoard = document.querySelector(".score-board");
const scoreVal = document.getElementById("score");
const questionBox = document.querySelector(".question-box");
const optionBox = document.querySelector(".option-wrapper");
const playBtn = document.querySelector(".play-btn");
const exitBtn = document.querySelector(".exit-btn");

/******************************* */

const optionsBtns = document.querySelectorAll(".option-text"); // for all 4 option's text

const optionsSelection = document.querySelectorAll("#opt-btn");

/********************************* */

function playSetup() {
  SCORE = 0;
  RAND_QUES = -1;
  playBtn.style.display = "block";
  questionBox.style.display = "none";
  optionBox.style.display = "none";
  exitBtn.style.display = "none";
  scoreBoard.style.display = "none";
  NUMS = numArray(0, 6); // for 7 total questions
}

function startGame() {
  scoreVal.innerHTML = SCORE;
  questionBox.style.display = "block";
  optionBox.style.display = "flex";
  exitBtn.style.display = "block";
  scoreBoard.style.display = "block";
  question.quizPlay();
}

playBtn.addEventListener("click", function () {
  this.style.display = "none";
  startGame();
});

exitBtn.addEventListener("click", function () {
  playSetup();
});

/*********** Question Section  */

let questionColl = [];
let optionColl = [];
const answerColl = [1, 2, 3, 3, 0, 2, 1]; // correct answer of the following quiz questions

/*************** questions ****** */

questionColl[0] = "Cruce peatonal es:";
optionColl[0] = {
  options: [
    "Parte de la vereda donde se debe esperar hasta poder cruzar",
    "Parte de la calle habilitada para ser atravesada por peatones",
    "Las señalizadas con franjas blancas paralelas",
    "Los llamados “lomos de burro”",
  ],
};

questionColl[1] =
  "¿Qué luces deben llevar encendidas un vehículo durante la noche en las áreas urbanas? ";
optionColl[1] = {
  options: [
    "Luces altas o bajas de acuerdo a la situación del tránsito",
    "Cualquiera indistintamente",
    "Solo A y C son correctas",
    "Luces de posición",
  ],
};

questionColl[2] = "Adelantamiento correcto es una maniobra realizada:";
optionColl[2] = {
  options: [
    "Nunca se debe adelantar",
    "Por la banquina si la hubiera",
    "Por el costado derecho del vehículo que se va a adelantar",
    "Por el costado izquierdo del vehículo que se va a adelantar",
  ],
};

questionColl[3] = "Un conductor al enfrentar una señal de PARE debe:";
optionColl[3] = {
  options: [
    "Seguir su marcha a la misma velocidad",
    "Reducir la velocidad y detenerse si fuera necesario",
    "Reducir un poco la velocidad y continuar",
    "Detener totalmente la marcha",
  ],
};

questionColl[4] =
  "¿Cuál es la edad mínima para poder viajar en el asiento delantero del vehículo?";
optionColl[4] = {
  options: ["12 años", "6 años", "1 año", "Ninguna es correcta"],
};

questionColl[5] = "Ante la proximidad de un vehículo de emergencia usted:";
optionColl[5] = {
  options: [
    "Aumenta la velocid",
    "Hace cambio de luces y toca la bocina para avisar al resto de los conductores",
    "Despeja rápidamente la calzada permaneciendo detenido donde no moleste",
    "Ninguna es correcta",
  ],
};

questionColl[6] =
  "Cuando va detrás de un vehículo durante la noche deben usarse las luces cortas";
optionColl[6] = {
  options: [
    "En ningún momento",
    "Cuando la distancia haga innecesarias las luces largas",
    "Cuando la visibilidad sea muy buena",
    "Ninguna de las anteriores es correcta",
  ],
};

let quizQuestion = function (question, optionList, correctAns) {
  this.question = question;
  this.optionList = optionList;
  this.correctAns = correctAns;
};

let question = new quizQuestion(questionColl, optionColl, answerColl);

function numArray(start, end) {
  let numsList = [];
  for (let i = start; i <= end; i++) {
    numsList.push(i);
  }
  return numsList;
}

function randValueGen(min, max) {
  let temp = Math.random() * (max - min + 1);
  let result = Math.floor(temp) + min;
  return result;
}

quizQuestion.prototype.quizPlay = function () {
  if (NUMS.length === 0) {
    document.getElementById("question").innerHTML = "Has completado el quiz.";
    optionBox.style.display = "none";
    return;
  }

  let randIndex = randValueGen(0, NUMS.length - 1);
  RAND_QUES = NUMS[randIndex];

  NUMS.splice(randIndex, 1);

  document.getElementById("question").innerHTML = this.question[RAND_QUES];

  this.optionList[RAND_QUES].options.forEach(function (option, idx) {
    optionsBtns[idx].innerHTML = option;
  });
};

optionsSelection.forEach(function (optionSelected, index) {
  optionSelected.addEventListener("click", function () {
    let userAns = parseInt(this.textContent) - 1;

    optionsSelection.forEach(function (option) {
      option.disabled = true;
    });

    question.checkAnswer(userAns);
  });
});

quizQuestion.prototype.checkAnswer = function (userAns) {
  optionsSelection[userAns].style.background = "white";
  optionsSelection[userAns].style.color = "black";

  let correctAns = question.correctAns[RAND_QUES];
  if (userAns === correctAns) {
    correctAnsUpdate();
  } else {
    incorrectAnsUpdate();
  }
};

function correctAnsUpdate() {
  document.getElementById("question").style.color = "green";
  document.getElementById("question").innerHTML = "Correcto!";
  SCORE++;
  scoreVal.innerHTML = SCORE;

  setTimeout(contdPlay, 1000);
}

function incorrectAnsUpdate() {
  document.getElementById("question").style.color = "red";
  document.getElementById("question").innerHTML = "Incorrecto!";

  setTimeout(contdPlay, 1000);
}

// for continuous play

function contdPlay() {
  optionsSelection.forEach(function (option) {
    option.disabled = false;
    option.style.background = "black";
    option.style.color = "white";
  });

  document.getElementById("question").style.color = "black";

  question.quizPlay();
}

playSetup();
